//
//  LosingViewController.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/26/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LosingViewController : UIViewController
@property (nonatomic) int boardSize;
@property (nonatomic, strong) NSArray *_products;
- (IBAction)tryAgainButtonPressed:(id)sender;
- (IBAction)newGameButtonPressed:(id)sender;
- (IBAction)menuButtonPressed:(id)sender;
@end
