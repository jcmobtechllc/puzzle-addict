//
//  JCTileBoard.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/18/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JCTileBoard : NSObject

@property (nonatomic) NSInteger size;
@property (nonatomic, strong) NSMutableArray *tiles;

- (instancetype)initWithSize:(NSInteger)size;
- (void)setTileAtCoordinate:(CGPoint)coor with:(NSNumber *)number;
- (NSNumber *)tileAtCoordinate:(CGPoint)coor;

- (BOOL)canMoveTile:(CGPoint)coor;
- (CGPoint)shouldMove:(BOOL)move tileAtCoordinate:(CGPoint)coor;
- (BOOL)isAllTilesCorrect;

- (void)shuffle:(NSInteger)times;

@end
