//
//  GameCenterManager.m
//  JCPuzzle
//
//  Created by Christine Morris on 11/29/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import "GameCenterManager.h"
#import <GameKit/GameKit.h>
#import "AppSpecificValues.h"

@implementation GameCenterManager
@synthesize gameCenterEnabled;

- (BOOL) isGameCenterAvailable {
    // check for presence of GKLocalPlayer API
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // check if the device is running iOS 4.1 or later
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

- (void)authenticateLocalPlayerFromViewController:(UIViewController *)presentingViewController {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error) {
        if (viewController != nil) {
            [presentingViewController presentViewController:viewController animated:YES completion:nil];
        } else {
            if ([GKLocalPlayer localPlayer].authenticated) {
                self.gameCenterEnabled = YES;
                
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *returnedLeaderboardIdentifier, NSError *error) {
                    
                    if (error != nil) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                }];
            } else {
                self.gameCenterEnabled = NO;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Game Center is not enabled" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                alertView = nil;
            }
        }
    };
}

- (void)reportScore:(int)score toLeaderboardIdentifier:(NSString *)selectedLeaderboardIdentifier {
    GKScore *reportedScore = [[GKScore alloc] initWithLeaderboardIdentifier:selectedLeaderboardIdentifier];
    reportedScore.value = score;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:selectedLeaderboardIdentifier]) {
        NSArray *components = [[[NSUserDefaults standardUserDefaults] valueForKey:selectedLeaderboardIdentifier] componentsSeparatedByString:@"-"];
        if ([components[1] integerValue] < score) {
            score = [components[1] integerValue];
        }
    }
    
    [GKScore reportScores:@[reportedScore] withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@-%d", selectedLeaderboardIdentifier, score] forKey:selectedLeaderboardIdentifier];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            if ([[NSUserDefaults standardUserDefaults] valueForKey:selectedLeaderboardIdentifier]) {
                [[NSUserDefaults standardUserDefaults] setValue:nil forKey:selectedLeaderboardIdentifier];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }];
}

- (void)showLeaderboardFromViewController:(UIViewController *)viewController withLeaderBoardIdentifier:(NSString *)selectedLeaderboardIdentifier {
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    gcViewController.gameCenterDelegate = self;
    gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
    gcViewController.leaderboardIdentifier = selectedLeaderboardIdentifier;
    
    [viewController presentViewController:gcViewController animated:YES completion:nil];
}

+ (GameCenterManager *)sharedInstance {
    static GameCenterManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GameCenterManager alloc] init];
    });
    
    return sharedInstance;
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
