//
//  JCTileBoardView.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/18/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JCTileBoardView, JCTileBoard;

@protocol JCTileBoardViewDelegate <NSObject>

@optional
- (void)tileBoardViewDidFinished:(JCTileBoardView *)tileBoardView;
- (void)tileBoardView:(JCTileBoardView *)tileBoardView tileDidMove:(CGPoint)position;

@end

@interface JCTileBoardView : UIView

@property (assign, nonatomic) IBOutlet id<JCTileBoardViewDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *tiles;
@property (strong, nonatomic) JCTileBoard *board;
- (instancetype)initWithImage:(UIImage *)image size:(NSInteger)size frame:(CGRect)frame;
- (void)playWithImage:(UIImage *)image size:(NSInteger)size;
- (void)playWithImage:(UIImage *)image withTiles:(NSMutableArray *)currentTiles size:(NSInteger)size;
- (void)shuffleTimes:(NSInteger)times;
- (void)drawTiles;
- (void)drawSavedTiles;
- (void)orderingTiles;

@end
