//
//  GameCenterManager.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/29/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameKit/GameKit.h"

@interface GameCenterManager : NSObject <GKGameCenterControllerDelegate>
@property (nonatomic) BOOL gameCenterEnabled;

- (BOOL)isGameCenterAvailable;
- (void)authenticateLocalPlayerFromViewController:(UIViewController *)presentingViewController;
- (void)reportScore:(int)score toLeaderboardIdentifier:(NSString *)selectedLeaderboardIdentifier;
- (void)showLeaderboardFromViewController:(UIViewController *)viewController withLeaderBoardIdentifier:(NSString *)selectedLeaderboardIdentifier;
+ (GameCenterManager *)sharedInstance;
@end
