//
//  MenuViewController.m
//  JCPuzzle
//
//  Created by Christine Morris on 11/21/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"
#import "JCTileBoardView.h"
#import "GameKit/GameKit.h"
#import <iAd/iAd.h>
#import "GameCenterManager.h"
#import "AppSpecificValues.h"
#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>
#import "Reachability.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define KPhotoGalleryTag 100
#define KChristmasTag 200

@interface MenuViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ADBannerViewDelegate> {
    BOOL _bannerIsVisible;
    ADBannerView *_adBanner;
}
@property (nonatomic) int photoMin;
@property (nonatomic) int photoMax;
@property (nonatomic, strong) NSMutableArray *shownPhotos;
@property (nonatomic, strong) NSMutableArray *christmasShownPhotos;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int counter;
@property (nonatomic) BOOL switchStatus;
@end

@implementation MenuViewController
@synthesize photoMin, photoMax;
@synthesize shownPhotos;
@synthesize christmasShownPhotos;
@synthesize fadeLabelSwitchLabel;
@synthesize segmentControl;
@synthesize timer;
@synthesize counter;
@synthesize switchStatus;
@synthesize resumeButton;
@synthesize frameView;
@synthesize _products;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.resumeButton.hidden = YES;
    
    //In-app purchase
    if (![_products count]) {
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus != NotReachable) {
            [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                if (success) {
                    _products = products;
                }
            }];
        }
    }
    
    //Authenticate User to game center
    if ([[GameCenterManager sharedInstance] isGameCenterAvailable]) {
        if (![[GameCenterManager sharedInstance] gameCenterEnabled]) {
            [[GameCenterManager sharedInstance] authenticateLocalPlayerFromViewController:self];
        }
    }
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"menuBoardSize"]) {
        [self.segmentControl setSelectedSegmentIndex:0];
        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"menuBoardSize"];
    }
    
    self.frameView.layer.cornerRadius = 10.0f;
    self.frameView.layer.borderWidth = 4.0f;
    self.frameView.layer.borderColor = [[UIColor colorWithRed:(222/255.0) green:(45/255.0) blue:(46/255.0) alpha:1.0] CGColor];
    
    self.photoMin = 1;
    self.photoMax = 50;
    
    self.shownPhotos = [[[NSUserDefaults standardUserDefaults] valueForKey:@"shownPhotos"] mutableCopy];
    if(!self.shownPhotos) {
        self.shownPhotos = [[NSMutableArray alloc] init];
    }
    
    self.christmasShownPhotos = [[[NSUserDefaults standardUserDefaults] valueForKey:@"christmasShownPhotos"] mutableCopy];
    if(!self.christmasShownPhotos) {
        self.christmasShownPhotos = [[NSMutableArray alloc] init];
    }
    
    NSString *timerSettingOn = [[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"];
    if (![timerSettingOn length] || [timerSettingOn isEqualToString:@"Yes"]) {
        fadeLabelSwitchLabel.on = YES;
        self.switchStatus = YES;
        if (![timerSettingOn length]) {
            [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"timerSettingOn"];
        }
    } else {
        fadeLabelSwitchLabel.on = NO;
        self.switchStatus = NO;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSArray *segmentControlArray = self.segmentControl.subviews;
    self.segmentControl.layer.borderColor = [[UIColor colorWithRed:(233/255.0) green:(86/255.0) blue:(37/255.0) alpha:1.0] CGColor];

    [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"CopperPlate-Bold" size:20.0f]} forState:UIControlStateSelected];
    [self.segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"CopperPlate-Bold" size:15.0f]} forState:UIControlStateNormal];
    
    [[segmentControlArray objectAtIndex:0] setBackgroundColor:[UIColor colorWithRed:(54/255.0) green:(109/255.0) blue:(236/255.0) alpha:1.0]];

    [[segmentControlArray objectAtIndex:1] setBackgroundColor:[UIColor colorWithRed:(66/255.0) green:(122/255.0) blue:(255/255.0) alpha:1.0]];
    
    [[segmentControlArray objectAtIndex:2] setBackgroundColor:[UIColor colorWithRed:(93/255.0) green:(155/255.0) blue:(255/255.0) alpha:1.0]];
    
    int menuBoardSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"menuBoardSize"] intValue];
    if (menuBoardSize >= 3) {
        [self.segmentControl setSelectedSegmentIndex:(menuBoardSize - 3)];
    }
    
    
    self.counter = 1;
    
    fadeLabelSwitchLabel.thumbImage = [UIImage imageNamed:@"StopWatch1"];
    fadeLabelSwitchLabel.trackMaskImage = [UIImage imageNamed:@"switchMask"];
    fadeLabelSwitchLabel.thumbHighlightImage = [UIImage imageNamed:@"StopWatch1"];
    fadeLabelSwitchLabel.trackImageOn = [UIImage imageNamed:@"switchBlue"];
    fadeLabelSwitchLabel.trackImageOff = [UIImage imageNamed:@"switchOrange"];
    fadeLabelSwitchLabel.onString = @"YEAH";
    fadeLabelSwitchLabel.offString = @"NOPE";
    fadeLabelSwitchLabel.onLabel.font = [UIFont boldSystemFontOfSize:11];
    fadeLabelSwitchLabel.offLabel.font = [UIFont boldSystemFontOfSize:11];
    fadeLabelSwitchLabel.onLabel.textColor = [UIColor whiteColor];
    fadeLabelSwitchLabel.offLabel.textColor = [UIColor whiteColor];
    fadeLabelSwitchLabel.onLabel.shadowColor = [UIColor colorWithRed:0.121569 green:0.600000 blue:0.454902 alpha:1.0];
    fadeLabelSwitchLabel.offLabel.shadowColor = [UIColor colorWithRed:0.796078 green:0.211765 blue:0.156863 alpha:1.0];
    fadeLabelSwitchLabel.onLabel.shadowOffset = CGSizeMake(0, 1.0);
    fadeLabelSwitchLabel.offLabel.shadowOffset = CGSizeMake(0, 1.0);
    fadeLabelSwitchLabel.labelsEdgeInsets = UIEdgeInsetsMake(1.0, 10.0, 1.0, 10.0);
    fadeLabelSwitchLabel.thumbInsetX = -3.0;
    fadeLabelSwitchLabel.thumbOffsetY = 0.0;
    
    __block MenuViewController *blockSafeSelf = self;
    [self.fadeLabelSwitchLabel setChangeHandler:^(BOOL on) {
        blockSafeSelf.counter = 1;
        if (on == NO) {
            blockSafeSelf.switchStatus = NO;
            [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"timerSettingOn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [blockSafeSelf.timer invalidate];
            [blockSafeSelf.fadeLabelSwitchLabel.thumbImageView addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"StopWatch%d", blockSafeSelf.counter]]]];
            for (UIView *subview in blockSafeSelf.fadeLabelSwitchLabel.thumbImageView.subviews) {
                if (subview != [blockSafeSelf.fadeLabelSwitchLabel.thumbImageView.subviews lastObject]) {
                    [subview removeFromSuperview];
                }
            }
        } else {
            blockSafeSelf.switchStatus = YES;
            [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"timerSettingOn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            blockSafeSelf.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:blockSafeSelf
                                                                 selector:@selector(updatePhoto) userInfo:nil repeats:YES];
        }
    }];
    
    if (self.switchStatus == YES) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                    selector:@selector(updatePhoto) userInfo:nil repeats:YES];
    }
}

- (IBAction)segmentControlValueChanged:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",self.segmentControl.selectedSegmentIndex+3] forKey:@"menuBoardSize"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CAShapeLayer* mask = [[CAShapeLayer alloc] init];
    mask.frame = CGRectMake(self.segmentControl.bounds.origin.x, self.segmentControl.bounds.origin.y, self.segmentControl.bounds.size.width, self.segmentControl.bounds.size.height);
    mask.path = [[UIBezierPath bezierPathWithRoundedRect:mask.frame cornerRadius:4] CGPath];
    self.segmentControl.layer.mask = mask;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"resumeButton"]) {
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"resumeButton"] isEqualToString:@"Yes"]) {
            self.resumeButton.hidden = NO;
        } else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"resumeButton"] isEqualToString:@"No"]) {
            self.resumeButton.hidden = YES;
        }
    } else {
        self.resumeButton.hidden = YES;
    }
    
    _adBanner = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 50)];
    _adBanner.delegate = self;
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
            
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            *stop = YES;
        }
    }];
}

- (void)updatePhoto {
    self.counter++;
    if (counter == 13) {
        self.counter = 1;
    }
    [fadeLabelSwitchLabel.thumbImageView addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"StopWatch%d", self.counter]]]];
    for (UIView *subview in fadeLabelSwitchLabel.thumbImageView.subviews) {
        if (subview != [fadeLabelSwitchLabel.thumbImageView.subviews lastObject]) {
            [subview removeFromSuperview];
        }
    }
}

- (IBAction)leaderBoardButtonPressed:(id)sender {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this process" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        alertView = nil;
    } else {
        NSString *leaderboardIdentifier = kEasyLeaderboardID;
        if (self.switchStatus == NO) {
            leaderboardIdentifier = kEasyMovesLeaderboardID;
        }
        if ((self.segmentControl.selectedSegmentIndex+3) == 4) {
            if (self.switchStatus == NO) {
                leaderboardIdentifier = kMediumMovesLeaderboardID;
            } else {
                leaderboardIdentifier = kMediumLeaderboardID;
            }
        } else if ((self.segmentControl.selectedSegmentIndex+3) == 5) {
            if (self.switchStatus == NO) {
                leaderboardIdentifier = kHardMovesLeaderboardID;
            } else {
                leaderboardIdentifier = kHardLeaderboardID;
            }
        }
        
        if ([[GameCenterManager sharedInstance] isGameCenterAvailable]) {
            if ([[GameCenterManager sharedInstance] gameCenterEnabled]) {
                [[GameCenterManager sharedInstance] showLeaderboardFromViewController:self withLeaderBoardIdentifier:leaderboardIdentifier];
            } else {
                //show game center log in page
                [[GameCenterManager sharedInstance] authenticateLocalPlayerFromViewController:self];
            }
        } else {
            //Error not available
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"Game Center is not available on your device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        }
    }
}

- (IBAction)restorePurchasesButtonPressed:(id)sender {
    [[IAPHelper sharedInstance] restoreCompletedTransactions];
}

- (IBAction)rateButtonPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id945224253"]];
}

- (IBAction)newGameButtonPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select the operation to proceed?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Random Photo", @"Photo From Gallery", @"Christmas Photos", nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0) {
        UIStoryboard *storyboard;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        } else {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        }
        
        int randNum;
        if ([self.shownPhotos count] && [self.shownPhotos count] != self.photoMax) {
            randNum = arc4random() % (self.photoMax - self.photoMin+1) + self.photoMin; //create the random number.
            while ([self.shownPhotos containsObject:[NSString stringWithFormat:@"%d", randNum]]) {
                randNum = arc4random() % (self.photoMax - self.photoMin+1) + self.photoMin; //create the random number.
            }
        } else {
            if ([self.shownPhotos count] == self.photoMax) {
                self.shownPhotos = [[NSMutableArray alloc] init];
            }
            randNum = arc4random() % (self.photoMax - self.photoMin+1) + self.photoMin; //create the random number.
        }
        [self.shownPhotos addObject:[NSString stringWithFormat:@"%d", randNum]];
        [[NSUserDefaults standardUserDefaults] setValue:self.shownPhotos forKey:@"shownPhotos"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"Photo%d.jpg", randNum] forKey:@"savedPhoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        vc.boardImage = [UIImage imageNamed:[NSString stringWithFormat:@"Photo%d.jpg", randNum]];
        vc.boardSize = self.segmentControl.selectedSegmentIndex+3;
        vc._products = self._products;
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
            vc.timerOn = YES;
        } else {
            vc.timerOn = NO;
        }
        
        [self presentViewController:vc animated:YES completion:nil];
    } else if(buttonIndex == 1) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:KPhotoFromGalleryInAppPurchaseID]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
        } else {
            //Buy
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Unlock the ability to play your own photos for only $0.99" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock", nil];
            alertView.tag = KPhotoGalleryTag;
            [alertView show];
            alertView = nil;
        }
    } else if (buttonIndex == 2) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:kChristmasPhotosInAppPurchaseID]) {
            //Purchased (Pick picture)
            UIStoryboard *storyboard;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            } else {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            }
            
            int randNum;
            int min = 1;
            int max = 20;
            if ([self.christmasShownPhotos count] && [self.christmasShownPhotos count] != max) {
                randNum = arc4random() % (max - min+1) + min; //create the random number.
                while ([self.christmasShownPhotos containsObject:[NSString stringWithFormat:@"%d", randNum]]) {
                    randNum = arc4random() % (max - min+1) + min; //create the random number.
                }
            } else {
                if ([self.christmasShownPhotos count] == max) {
                    self.christmasShownPhotos = [[NSMutableArray alloc] init];
                }
                randNum = arc4random() % (max - min+1) + min; //create the random number.
            }
            [self.christmasShownPhotos addObject:[NSString stringWithFormat:@"%d", randNum]];
            [[NSUserDefaults standardUserDefaults] setValue:self.christmasShownPhotos forKey:@"christmasShownPhotos"];
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"Christmas%d.jpg", randNum] forKey:@"savedPhoto"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            vc.boardImage = [UIImage imageNamed:[NSString stringWithFormat:@"Christmas%d.jpg", randNum]];
            vc.boardSize = self.segmentControl.selectedSegmentIndex+3;
            vc._products = self._products;
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
                vc.timerOn = YES;
            } else {
                vc.timerOn = NO;
            }
            
            [self presentViewController:vc animated:YES completion:nil];
        } else {
            //Buy
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Unlock 20 Christmas Photos for only $0.99" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock", nil];
            alertView.tag = KChristmasTag;
            [alertView show];
            alertView = nil;
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (alertView.tag == KPhotoGalleryTag) {
        if (buttonIndex == 1) {
            if (networkStatus == NotReachable) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this purchase" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                alertView = nil;
            } else {
                if ([_products count]) {
                    SKProduct *photoFromGalleryProduct;
                    for (SKProduct *product in _products) {
                        if ([product.productIdentifier isEqualToString:KPhotoFromGalleryInAppPurchaseID]) {
                            photoFromGalleryProduct = product;
                            break;
                        }
                    }
                    
                    NSLog(@"Buying %@...", photoFromGalleryProduct.productIdentifier);
                    [[IAPHelper sharedInstance] buyProduct:photoFromGalleryProduct];
                } else {
                    if (networkStatus != NotReachable) {
                        [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                            if (success) {
                                _products = products;
                                
                                SKProduct *photoFromGalleryProduct;
                                for (SKProduct *product in _products) {
                                    if ([product.productIdentifier isEqualToString:KPhotoFromGalleryInAppPurchaseID]) {
                                        photoFromGalleryProduct = product;
                                        break;
                                    }
                                }
                                
                                NSLog(@"Buying %@...", photoFromGalleryProduct.productIdentifier);
                                [[IAPHelper sharedInstance] buyProduct:photoFromGalleryProduct];
                            }
                        }];
                    }
                }
            }
        }
    } else if (alertView.tag == KChristmasTag) {
        if (buttonIndex == 1) {
            if (networkStatus == NotReachable) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this purchase" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                alertView = nil;
            } else {
                if ([_products count]) {
                    SKProduct *christimasPhotosProduct;
                    for (SKProduct *product in _products) {
                        if ([product.productIdentifier isEqualToString:kChristmasPhotosInAppPurchaseID]) {
                            christimasPhotosProduct = product;
                            break;
                        }
                    }
                    
                    NSLog(@"Buying %@...", christimasPhotosProduct.productIdentifier);
                    [[IAPHelper sharedInstance] buyProduct:christimasPhotosProduct];
                } else {
                    if (networkStatus != NotReachable) {
                        [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                            if (success) {
                                _products = products;
                                
                                SKProduct *christimasPhotosProduct;
                                for (SKProduct *product in _products) {
                                    if ([product.productIdentifier isEqualToString:kChristmasPhotosInAppPurchaseID]) {
                                        christimasPhotosProduct = product;
                                        break;
                                    }
                                }
                                
                                NSLog(@"Buying %@...", christimasPhotosProduct.productIdentifier);
                                [[IAPHelper sharedInstance] buyProduct:christimasPhotosProduct];
                            }
                        }];
                    }
                }
            }
        }
    }
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"savedPhoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/savedPhoto.jpg",docDir];
    NSData *photoData = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage, 0)];
    [photoData writeToFile:pngFilePath atomically:YES];
    
    ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc.boardImage = chosenImage;
    vc.boardSize = self.segmentControl.selectedSegmentIndex+3;
    vc._products = self._products;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
        vc.timerOn = YES;
    } else {
        vc.timerOn = NO;
    }
    
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)resumeButtonPressed:(id)sender {
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    UIImage *boardImage;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"savedPhoto"]) {
        boardImage = [UIImage imageNamed:[[NSUserDefaults standardUserDefaults] valueForKey:@"savedPhoto"]];
    } else {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *jpgFilePath = [NSString stringWithFormat:@"%@/savedPhoto.jpg",docDir];
        boardImage = [UIImage imageWithContentsOfFile:jpgFilePath];
    }
    
    NSMutableArray *tilesEncodedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"tiles"];
    NSMutableArray *tilesDecodedArray = [[NSMutableArray alloc] init];
    for (NSData *encodedImageView in tilesEncodedArray) {
        UIImageView *decodedImageView = [NSKeyedUnarchiver unarchiveObjectWithData:encodedImageView];
        [tilesDecodedArray addObject:decodedImageView];
    }
    
    NSMutableArray *tilesArrayEncodedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"tilesArray"];
    NSMutableArray *tilesArrayDecodedArray = [[NSMutableArray alloc] init];
    for (NSData *encodedTileArray in tilesArrayEncodedArray) {
        NSMutableArray *decodedTileArray = [NSKeyedUnarchiver unarchiveObjectWithData:encodedTileArray];
        [tilesArrayDecodedArray addObject:decodedTileArray];
    }
    
    ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc.tilesArray = tilesArrayDecodedArray;
    vc.boardSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"boardSize"] integerValue];
    vc.boardImage = boardImage;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timer"] isEqualToString:@"Yes"]) {
        vc.secondsLeft = [[[NSUserDefaults standardUserDefaults] valueForKey:@"secondsLeft"] intValue];
        vc.timerOn = YES;
    } else {
        vc.timerOn = NO;
    }
    
    vc.tiles = tilesDecodedArray;
    vc.steps = [[[NSUserDefaults standardUserDefaults] valueForKey:@"steps"] integerValue];
    vc.resumed = YES;
    vc._products = self._products;
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!_bannerIsVisible)
    {
        // If banner isn't part of view hierarchy, add it
        if (_adBanner.superview == nil)
        {
            [self.view addSubview:_adBanner];
        }
        
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        
        // Assumes the banner view is just off the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = YES;
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to retrieve ad");
    
    if (_bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        
        // Assumes the banner view is placed at the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
