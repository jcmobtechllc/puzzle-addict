//
//  ViewController.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/18/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JCTileBoardView;

@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet JCTileBoardView *board;
@property (nonatomic, strong) NSMutableArray *tiles;
@property (nonatomic, strong) NSMutableArray *tilesArray;
@property (weak, nonatomic) IBOutlet UIButton *imageViewButton;
@property (nonatomic, strong) UIImage *boardImage;
@property (nonatomic) NSInteger boardSize;
@property (nonatomic) NSInteger steps;
@property (nonatomic) int secondsLeft;
@property (nonatomic) BOOL resumed;
@property (nonatomic) BOOL timerOn;
@property (nonatomic, strong) NSArray *_products;

- (IBAction)menuPauseButtonPressed:(id)sender;
- (IBAction)shuffleButtonPressed:(id)sender;
- (IBAction)leaderBoardButtonPressed:(id)sender;
@end

