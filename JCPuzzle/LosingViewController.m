//
//  LosingViewController.m
//  JCPuzzle
//
//  Created by Christine Morris on 11/26/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import "LosingViewController.h"
#import "MenuViewController.h"
#import "ViewController.h"
#import "AppSpecificValues.h"
#import <iAd/iAd.h>
#import "Reachability.h"
#import "IAPHelper.h"

#define KPhotoGalleryTag 100
#define KChristmasTag 200

@interface LosingViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ADBannerViewDelegate> {
    BOOL _bannerIsVisible;
    ADBannerView *_adBanner;
    NSArray *_products;
}

@end

@implementation LosingViewController
@synthesize boardSize;
@synthesize _products;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"resumeButton"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _adBanner = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 50)];
    _adBanner.delegate = self;
    
    //In-app purchase
    if (![_products count]) {
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus != NotReachable) {
            [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                if (success) {
                    _products = products;
                }
            }];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.navigationController.navigationBar.hidden = YES;
    self.parentViewController.view.superview.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-(self.view.frame.size.width/2), ([UIScreen mainScreen].bounds.size.height/2)-(self.view.frame.size.height/2), self.view.frame.size.width, self.view.frame.size.height);
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            *stop = YES;
        }
    }];
}

- (IBAction)tryAgainButtonPressed:(id)sender {
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    UIImage *boardImage;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"savedPhoto"]) {
        boardImage = [UIImage imageNamed:[[NSUserDefaults standardUserDefaults] valueForKey:@"savedPhoto"]];
    } else {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *jpgFilePath = [NSString stringWithFormat:@"%@/savedPhoto.jpg",docDir];
        boardImage = [UIImage imageWithContentsOfFile:jpgFilePath];
    }
    
    ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc.boardSize = self.boardSize;
    vc.boardImage = boardImage;
    vc._products = self._products;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
        vc.timerOn = YES;
    } else {
        vc.timerOn = NO;
    }
    
    vc.steps = 0;
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)newGameButtonPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select the operation to proceed?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Random Photo", @"Photo From Gallery", @"Christmas Photos", nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        UIStoryboard *storyboard;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        } else {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        }
        
        int randNum;
        int photoMin = 1;
        int photoMax = 50;
        
        NSMutableArray *shownPhotos = [[[NSUserDefaults standardUserDefaults] valueForKey:@"shownPhotos"] mutableCopy];
        if(!shownPhotos) {
            shownPhotos = [[NSMutableArray alloc] init];
        }
        
        if ([shownPhotos count] && [shownPhotos count] != photoMax) {
            randNum = arc4random() % (photoMax - photoMin+1) + photoMin; //create the random number.
            while ([shownPhotos containsObject:[NSString stringWithFormat:@"%d", randNum]]) {
                randNum = arc4random() % (photoMax - photoMin+1) + photoMin; //create the random number.
            }
        } else {
            if ([shownPhotos count] == photoMax) {
                shownPhotos = [[NSMutableArray alloc] init];
            }
            randNum = arc4random() % (photoMax - photoMin+1) + photoMin; //create the random number.
        }
        [shownPhotos addObject:[NSString stringWithFormat:@"%d", randNum]];
        [[NSUserDefaults standardUserDefaults] setValue:shownPhotos forKey:@"shownPhotos"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"Photo%d.jpg", randNum] forKey:@"savedPhoto"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        vc.boardImage = [UIImage imageNamed:[NSString stringWithFormat:@"Photo%d.jpg", randNum]];
        vc.boardSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"menuBoardSize"] integerValue];
        vc._products = self._products;
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
            vc.timerOn = YES;
        } else {
            vc.timerOn = NO;
        }
        
        [self presentViewController:vc animated:YES completion:nil];
    } else if(buttonIndex == 1) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:KPhotoFromGalleryInAppPurchaseID]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
        } else {
            //Buy
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Unlock the ability to play your own photos for only $0.99" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock", nil];
            alertView.tag = KPhotoGalleryTag;
            [alertView show];
            alertView = nil;
        }
    } else if (buttonIndex == 2) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:kChristmasPhotosInAppPurchaseID]) {
            //Purchased (Pick picture)
            UIStoryboard *storyboard;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            } else {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            }
            
            int randNum;
            int min = 1;
            int max = 20;
            
            NSMutableArray *christmasShownPhotos = [[[NSUserDefaults standardUserDefaults] valueForKey:@"christmasShownPhotos"] mutableCopy];
            if(!christmasShownPhotos) {
                christmasShownPhotos = [[NSMutableArray alloc] init];
            }
            if ([christmasShownPhotos count] && [christmasShownPhotos count] != max) {
                randNum = arc4random() % (max - min+1) + min; //create the random number.
                while ([christmasShownPhotos containsObject:[NSString stringWithFormat:@"%d", randNum]]) {
                    randNum = arc4random() % (max - min+1) + min; //create the random number.
                }
            } else {
                if ([christmasShownPhotos count] == max) {
                    christmasShownPhotos = [[NSMutableArray alloc] init];
                }
                randNum = arc4random() % (max - min+1) + min; //create the random number.
            }
            [christmasShownPhotos addObject:[NSString stringWithFormat:@"%d", randNum]];
            [[NSUserDefaults standardUserDefaults] setValue:christmasShownPhotos forKey:@"christmasShownPhotos"];
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"Christmas%d.jpg", randNum] forKey:@"savedPhoto"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            vc.boardImage = [UIImage imageNamed:[NSString stringWithFormat:@"Christmas%d.jpg", randNum]];
            vc.boardSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"menuBoardSize"] integerValue];
            vc._products = self._products;
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
                vc.timerOn = YES;
            } else {
                vc.timerOn = NO;
            }
            
            [self presentViewController:vc animated:YES completion:nil];
        } else {
            //Buy
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Unlock 20 Christmas Photos for only $0.99" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Unlock", nil];
            alertView.tag = KChristmasTag;
            [alertView show];
            alertView = nil;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (alertView.tag == KPhotoGalleryTag) {
        if (buttonIndex == 1) {
            if (networkStatus == NotReachable) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this purchase" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                alertView = nil;
            } else {
                if ([_products count]) {
                    SKProduct *photoFromGalleryProduct;
                    for (SKProduct *product in _products) {
                        if ([product.productIdentifier isEqualToString:KPhotoFromGalleryInAppPurchaseID]) {
                            photoFromGalleryProduct = product;
                            break;
                        }
                    }
                    
                    NSLog(@"Buying %@...", photoFromGalleryProduct.productIdentifier);
                    [[IAPHelper sharedInstance] buyProduct:photoFromGalleryProduct];
                } else {
                    if (networkStatus != NotReachable) {
                        [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                            if (success) {
                                _products = products;
                                
                                SKProduct *photoFromGalleryProduct;
                                for (SKProduct *product in _products) {
                                    if ([product.productIdentifier isEqualToString:KPhotoFromGalleryInAppPurchaseID]) {
                                        photoFromGalleryProduct = product;
                                        break;
                                    }
                                }
                                
                                NSLog(@"Buying %@...", photoFromGalleryProduct.productIdentifier);
                                [[IAPHelper sharedInstance] buyProduct:photoFromGalleryProduct];
                            }
                        }];
                    }
                }
            }
        }
    } else if (alertView.tag == KChristmasTag) {
        if (buttonIndex == 1) {
            if (networkStatus == NotReachable) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this purchase" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
                alertView = nil;
            } else {
                if ([_products count]) {
                    SKProduct *christimasPhotosProduct;
                    for (SKProduct *product in _products) {
                        if ([product.productIdentifier isEqualToString:kChristmasPhotosInAppPurchaseID]) {
                            christimasPhotosProduct = product;
                            break;
                        }
                    }
                    
                    NSLog(@"Buying %@...", christimasPhotosProduct.productIdentifier);
                    [[IAPHelper sharedInstance] buyProduct:christimasPhotosProduct];
                } else {
                    if (networkStatus != NotReachable) {
                        [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                            if (success) {
                                _products = products;
                                
                                SKProduct *christimasPhotosProduct;
                                for (SKProduct *product in _products) {
                                    if ([product.productIdentifier isEqualToString:kChristmasPhotosInAppPurchaseID]) {
                                        christimasPhotosProduct = product;
                                        break;
                                    }
                                }
                                
                                NSLog(@"Buying %@...", christimasPhotosProduct.productIdentifier);
                                [[IAPHelper sharedInstance] buyProduct:christimasPhotosProduct];
                            }
                        }];
                    }
                }
            }
        }
    }
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"savedPhoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/savedPhoto.jpg",docDir];
    NSData *photoData = [NSData dataWithData:UIImageJPEGRepresentation(chosenImage, 0)];
    [photoData writeToFile:pngFilePath atomically:YES];
    
    ViewController *vc = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc.boardImage = chosenImage;
    vc.boardSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"menuBoardSize"] integerValue];
    vc._products = self._products;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"timerSettingOn"] isEqualToString:@"Yes"]) {
        vc.timerOn = YES;
    } else {
        vc.timerOn = NO;
    }
    
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)menuButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"secondsLeft"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"boardSize"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"boardImage"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tiles"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tilesArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    MenuViewController *vc;
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5) {
        vc = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    } else if (IS_IPHONE_6 || IS_IPHONE_6P) {
        vc = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuViewController6"];
    }
    vc._products = self._products;
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!_bannerIsVisible)
    {
        // If banner isn't part of view hierarchy, add it
        if (_adBanner.superview == nil)
        {
            [self.view addSubview:_adBanner];
        }
        
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        
        // Assumes the banner view is just off the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = YES;
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to retrieve ad");
    
    if (_bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        
        // Assumes the banner view is placed at the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
