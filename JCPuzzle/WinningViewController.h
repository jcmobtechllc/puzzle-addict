//
//  WinningViewController.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/26/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WinningViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *scoreLabel;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *leaderBoardIdentifier;
@property (nonatomic, strong) NSArray *_products;

- (IBAction)menuButtonPressed:(id)sender;
- (IBAction)playAgainButtonPressed:(id)sender;
- (IBAction)leaderBoardButtonPressed:(id)sender;
@end
