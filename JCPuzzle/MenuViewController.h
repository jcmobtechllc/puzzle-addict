//
//  MenuViewController.h
//  JCPuzzle
//
//  Created by Christine Morris on 11/21/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTFadeSwitch.h"

@interface MenuViewController : UIViewController
@property (nonatomic, strong) IBOutlet TTFadeSwitch *fadeLabelSwitchLabel;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, strong) IBOutlet UIButton *resumeButton;
@property (nonatomic, strong) IBOutlet UIView *frameView;
@property (nonatomic, strong) NSArray *_products;

- (IBAction)newGameButtonPressed:(id)sender;
- (IBAction)resumeButtonPressed:(id)sender;
- (IBAction)leaderBoardButtonPressed:(id)sender;
- (IBAction)rateButtonPressed:(id)sender;
- (IBAction)restorePurchasesButtonPressed:(id)sender;
- (IBAction)segmentControlValueChanged:(id)sender;
@end
