//
//  ViewController.m
//  JCPuzzle
//
//  Created by Christine Morris on 11/18/14.
//  Copyright (c) 2014 JCMobTech, LLC. All rights reserved.
//

#import "ViewController.h"
#import "MenuViewController.h"
#import "JCTileBoardView.h"
#import "JCTileBoard.h"
#import "WinningViewController.h"
#import "LosingViewController.h"
#import <iAd/iAd.h>
#import "GameCenterManager.h"
#import "AppSpecificValues.h"
#import "Reachability.h"

static const NSTimeInterval AnimationSpeed = 0.2;

@interface ViewController () <JCTileBoardViewDelegate, ADBannerViewDelegate> {
    BOOL _bannerIsVisible;
    ADBannerView *_adBanner;
}

@property (weak, nonatomic) IBOutlet UILabel *stepsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepsLabel;
@property (weak, nonatomic) UIImageView *imageView;
@property (nonatomic) BOOL viewLoaded;
@property (nonatomic) BOOL stopBackgroundOperations;
//timer
@property (nonatomic, strong) NSTimer *counterTimer;
@property (nonatomic, strong) NSTimer *blinkingTimer;
-(void)updateCounter:(NSTimer *)theTimer;
-(void)countdownTimer;
@end

@implementation ViewController
@synthesize boardSize;
@synthesize viewLoaded;
@synthesize counterTimer;
@synthesize blinkingTimer;
@synthesize secondsLeft;
@synthesize resumed;
@synthesize tiles, tilesArray;
@synthesize timerOn;
@synthesize _products;
@synthesize stopBackgroundOperations;

int minutes, seconds;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.imageViewButton.layer.borderWidth = 2.0f;
    self.imageViewButton.layer.borderColor = [[UIColor colorWithRed:(129/255.0) green:(26/255.0) blue:(21/255.0) alpha:1] CGColor];
    [self.imageViewButton setImage:self.boardImage forState:UIControlStateNormal];
    
    self.viewLoaded = NO;
    self.stopBackgroundOperations = NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if (viewLoaded == NO) {
        if (self.resumed == YES) {
            [self restoreTiles];
        } else {
            [self restart:nil];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveGameState) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveGameState) name:UIApplicationWillTerminateNotification object:nil];
    
    self.viewLoaded = YES;
    self.resumed = NO;
    
    _adBanner = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 50)];
    _adBanner.delegate = self;
    
    if (self.timerOn == YES) {
        if (!self.secondsLeft) {
            if (self.boardSize == 3) {
                secondsLeft = 120;
            } else if (self.boardSize == 4) {
                secondsLeft = 180;
            } else if (self.boardSize == 5) {
                secondsLeft = 240;
            }
        }
        
        minutes = (secondsLeft % 3600) / 60;
        seconds = (secondsLeft %3600) % 60;
        
        if (self.secondsLeft <= 10) {
            self.blinkingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(blinkTimer:) userInfo:nil repeats:YES];
        }
        
        self.stepsTitleLabel.text = @"Timer";
        self.stepsLabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
        [self countdownTimer];
    } else {
        self.stepsTitleLabel.text = @"Moves";
        self.stepsLabel.text = [NSString stringWithFormat:@"%ld", (long)self.steps];
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *tilesArchivedArray = [NSMutableArray arrayWithCapacity:self.board.tiles.count];
        for (UIImageView *imageView in self.board.tiles) {
            NSData *encodedImageView = [NSKeyedArchiver archivedDataWithRootObject:imageView];
            [tilesArchivedArray addObject:encodedImageView];
        }
        [[NSUserDefaults standardUserDefaults] setObject:tilesArchivedArray forKey:@"tiles"];
        
        NSMutableArray *tilesArrayArchivedArray = [NSMutableArray arrayWithCapacity:self.board.board.tiles.count];
        for (NSMutableArray *tileArray in self.board.board.tiles) {
            NSData *encodedMutableArray = [NSKeyedArchiver archivedDataWithRootObject:tileArray];
            [tilesArrayArchivedArray addObject:encodedMutableArray];
        }
        [[NSUserDefaults standardUserDefaults] setObject:tilesArrayArchivedArray forKey:@"tilesArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}

- (void)saveGameState {
    [self.counterTimer invalidate];
    [self.blinkingTimer invalidate];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"resumeButton"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d", self.secondsLeft] forKey:@"secondsLeft"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld", (long)self.boardSize] forKey:@"boardSize"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld", (long)self.steps] forKey:@"steps"];
        if (self.timerOn == YES) {
            [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"timer"];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"timer"];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIStoryboard *storyboard;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            } else {
                storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            }
            
            MenuViewController *vc;
            
            if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5) {
                vc = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            } else if (IS_IPHONE_6 || IS_IPHONE_6P) {
                vc = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuViewController6"];
            }
            vc._products = self._products;
            
            [self presentViewController:vc animated:YES completion:nil];
        });
    });
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!_bannerIsVisible)
    {
        // If banner isn't part of view hierarchy, add it
        if (_adBanner.superview == nil)
        {
            [self.view addSubview:_adBanner];
        }
        
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        
        // Assumes the banner view is just off the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = YES;
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to retrieve ad");
    
    if (_bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        
        // Assumes the banner view is placed at the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = NO;
    }
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    //pause game
    [self.counterTimer invalidate];
    [self.blinkingTimer invalidate];
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    //unpause game
    self.counterTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
    if (self.secondsLeft <= 10) {
        self.blinkingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(blinkTimer:) userInfo:nil repeats:YES];
    }
}

- (void)updateCounter:(NSTimer *)theTimer {
    if(self.secondsLeft > 0 && self.timerOn == YES) {
        if (self.secondsLeft == 10) {
            self.blinkingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(blinkTimer:) userInfo:nil repeats:YES];
        }
        secondsLeft -- ;
        minutes = (secondsLeft % 3600) / 60;
        seconds = (secondsLeft %3600) % 60;
        self.stepsLabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    } else {
        //lost
        [self.counterTimer invalidate];
        [self.blinkingTimer invalidate];
        
        self.stopBackgroundOperations = YES;
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tiles"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tilesArray"];
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"secondsLeft"];
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"steps"];
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"timer"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        } else {
            storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        }
        
        LosingViewController *losingViewController = (LosingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LosingViewController"];
        losingViewController.boardSize = self.boardSize;
        losingViewController._products = self._products;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:losingViewController];
        navigationController.modalPresentationStyle = UIModalPresentationCustom;
        
        [self presentViewController:navigationController animated:NO completion:nil];
    }
}

- (IBAction)blinkTimer:(id)sender {
    if (self.stepsLabel.alpha == 0) {
        self.stepsLabel.alpha = 1;
    } else {
        self.stepsLabel.alpha = 0;
    }
}

-(void)countdownTimer{
    if([self.counterTimer isValid]) {
        [self.counterTimer invalidate];
    }
    self.counterTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuPauseButtonPressed:(id)sender {
    [self saveGameState];
}

- (IBAction)shuffleButtonPressed:(id)sender {
    [self.board shuffleTimes:100];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *tilesArchivedArray = [NSMutableArray arrayWithCapacity:self.board.tiles.count];
        for (UIImageView *imageView in self.board.tiles) {
            NSData *encodedImageView = [NSKeyedArchiver archivedDataWithRootObject:imageView];
            [tilesArchivedArray addObject:encodedImageView];
        }
        [[NSUserDefaults standardUserDefaults] setObject:tilesArchivedArray forKey:@"tiles"];
        
        NSMutableArray *tilesArrayArchivedArray = [NSMutableArray arrayWithCapacity:self.board.board.tiles.count];
        for (NSMutableArray *tileArray in self.board.board.tiles) {
            NSData *encodedMutableArray = [NSKeyedArchiver archivedDataWithRootObject:tileArray];
            [tilesArrayArchivedArray addObject:encodedMutableArray];
        }
        [[NSUserDefaults standardUserDefaults] setObject:tilesArrayArchivedArray forKey:@"tilesArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    });
}

#pragma mark - Actions
- (IBAction)restart:(UIButton *)sender {
    [self.board playWithImage:self.boardImage size:self.boardSize];
    [self.board shuffleTimes:100];
    self.steps = 0;
    
    [self hideImage];
}

- (IBAction)restoreTiles {
    [self.board playWithImage:self.boardImage withTiles:self.tiles size:self.boardSize];
    self.board.board.tiles = self.tilesArray;
    [self.board drawSavedTiles];
    
    [self hideImage];
}

- (void)showImage {
    UIImageView *originalImage = [[UIImageView alloc] initWithImage:self.boardImage];
    originalImage.frame = self.board.frame;
    originalImage.alpha = 0.0;
    
    [originalImage.layer setShadowColor:[UIColor blackColor].CGColor];
    [originalImage.layer setShadowOpacity:0.65];
    [originalImage.layer setShadowRadius:1.5];
    [originalImage.layer setShadowOffset:CGSizeMake(1.5, 1.5)];
    [originalImage.layer setShadowPath:[[UIBezierPath bezierPathWithRect:originalImage.layer.bounds] CGPath]];
    
    [self.view addSubview:originalImage];
    
    [UIView animateWithDuration:AnimationSpeed animations:^{
        originalImage.alpha = 1.0;
        self.imageView = originalImage;
        self.board.alpha = 0.0;
    } completion:nil];
}

- (void)hideImage
{
    if (!self.imageView) return;
    
    [UIView animateWithDuration:AnimationSpeed animations:^{
        self.imageView.alpha = 0.0;
        self.board.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self.imageView removeFromSuperview];
        self.imageView = nil;
    }];
}

- (IBAction)buttonTouchDown:(id)sender
{
    [self showImage];
}

- (IBAction)buttonTouchUpInside:(id)sender
{
    [self hideImage];
}

#pragma mark - Tile Board Delegate Method

- (void)tileBoardView:(JCTileBoardView *)tileBoardView tileDidMove:(CGPoint)position
{
    NSLog(@"tile move : %@", [NSValue valueWithCGPoint:position]);
    self.steps++;
    if (self.timerOn == NO) {
        self.stepsLabel.text = [NSString stringWithFormat:@"%ld", (long)self.steps];
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        if (self.stopBackgroundOperations == NO) {
            NSMutableArray *tilesArchivedArray = [NSMutableArray arrayWithCapacity:self.board.tiles.count];
            for (UIImageView *imageView in self.board.tiles) {
                NSData *encodedImageView = [NSKeyedArchiver archivedDataWithRootObject:imageView];
                [tilesArchivedArray addObject:encodedImageView];
            }
            [[NSUserDefaults standardUserDefaults] setObject:tilesArchivedArray forKey:@"tiles"];
            
            NSMutableArray *tilesArrayArchivedArray = [NSMutableArray arrayWithCapacity:self.board.board.tiles.count];
            for (NSMutableArray *tileArray in self.board.board.tiles) {
                NSData *encodedMutableArray = [NSKeyedArchiver archivedDataWithRootObject:tileArray];
                [tilesArrayArchivedArray addObject:encodedMutableArray];
            }
            [[NSUserDefaults standardUserDefaults] setObject:tilesArrayArchivedArray forKey:@"tilesArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"resumeButton"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    });
}

- (void)tileBoardViewDidFinished:(JCTileBoardView *)tileBoardView {
    [self.counterTimer invalidate];
    [self.blinkingTimer invalidate];
    
    self.stopBackgroundOperations = YES;
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"boardImage"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tiles"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"tilesArray"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"secondsLeft"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"steps"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"boardSize"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"timer"];
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"savedPhoto"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"tile is completed");
//    [self showImage];
    
    int winnerScore = [self calculateScore];
    
    NSString *leaderboardIdentifier = @"";
    if ([[GameCenterManager sharedInstance] isGameCenterAvailable]) {
        if ([[GameCenterManager sharedInstance] gameCenterEnabled]) {
            leaderboardIdentifier = kEasyLeaderboardID;
            if (self.timerOn == NO) {
                leaderboardIdentifier = kEasyMovesLeaderboardID;
            }
            if (self.boardSize == 4) {
                if (self.timerOn == NO) {
                    leaderboardIdentifier = kMediumMovesLeaderboardID;
                } else {
                    leaderboardIdentifier = kMediumLeaderboardID;
                }
            } else if (self.boardSize == 5) {
                if (self.timerOn == NO) {
                    leaderboardIdentifier = kHardMovesLeaderboardID;
                } else {
                    leaderboardIdentifier = kHardLeaderboardID;
                }
            }
            
            [[GameCenterManager sharedInstance] reportScore:winnerScore toLeaderboardIdentifier:leaderboardIdentifier];
        }
    }
    
    NSString *finalScore = @"";
    if (self.timerOn) {
        if (winnerScore == 1) {
            finalScore = [NSString stringWithFormat:@"%d second", winnerScore];
        } else {
            finalScore = [NSString stringWithFormat:@"%d seconds", winnerScore];
        }
    } else {
        if (winnerScore == 1) {
            finalScore = [NSString stringWithFormat:@"%d move", winnerScore];
        } else {
            finalScore = [NSString stringWithFormat:@"%d moves", winnerScore];
        }
    }
    
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    WinningViewController *winningViewController = (WinningViewController *)[storyboard instantiateViewControllerWithIdentifier:@"WinningViewController"];
    winningViewController.score = finalScore;
    winningViewController.leaderBoardIdentifier = leaderboardIdentifier;
    winningViewController._products = self._products;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:winningViewController];
    navigationController.modalPresentationStyle = UIModalPresentationCustom;
    
    [self presentViewController:navigationController animated:NO completion:nil];
}

- (int)calculateScore {
    int score = 0;
    if (self.timerOn == YES) {
        int initialTime = 0;
        if (self.boardSize == 3) {
            initialTime = 120;
        } else if (self.boardSize == 4) {
            initialTime = 180;
        } else if (self.boardSize == 5) {
            initialTime = 240;
        }
        score = initialTime - self.secondsLeft;
    } else {
        score = self.steps;
    }
    return score;
}

- (IBAction)leaderBoardButtonPressed:(id)sender {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"An Internet Connection is needed for this process" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        alertView = nil;
    } else {
        NSString *leaderboardIdentifier = kEasyLeaderboardID;
        if (self.timerOn == NO) {
            leaderboardIdentifier = kEasyMovesLeaderboardID;
        }
        if (self.boardSize == 4) {
            if (self.timerOn == NO) {
                leaderboardIdentifier = kMediumMovesLeaderboardID;
            } else {
                leaderboardIdentifier = kMediumLeaderboardID;
            }
        } else if (self.boardSize == 5) {
            if (self.timerOn == NO) {
                leaderboardIdentifier = kHardMovesLeaderboardID;
            } else {
                leaderboardIdentifier = kHardLeaderboardID;
            }
        }
        
        if ([[GameCenterManager sharedInstance] isGameCenterAvailable]) {
            if ([[GameCenterManager sharedInstance] gameCenterEnabled]) {
                [[GameCenterManager sharedInstance] showLeaderboardFromViewController:self withLeaderBoardIdentifier:leaderboardIdentifier];
            } else {
                //show game center log in page
                [[GameCenterManager sharedInstance] authenticateLocalPlayerFromViewController:self];
            }
        } else {
            //Error not available
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Invalid" message:@"Game Center is not available on your device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            alertView = nil;
        }
    }
}

@end
